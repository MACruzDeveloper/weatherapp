import 'react-native-gesture-handler';
import React, { useState, useEffect } from 'react';
import Moment from 'react-moment';
import { NavigationContainer } from '@react-navigation/native';
import { StyleSheet, Text, View, Image, ImageBackground, ScrollView, TextInput, SafeAreaView, FlatList, Button } from 'react-native';

export default function App() {
  const [location, setLocation] = useState({ latitude: 0, longitude: 0 })
  const [city, setCity] = useState('')
  const [weatherCurrent, setWeatherCurrent] = useState(null)
  const [weatherDaily, setWeatherDaily] = useState([])
  const [weatherHourly, setWeatherHourly] = useState([])
  const [text, setText] = useState('')
  const [predictions, setPredictions] = useState([])
  const [isLoading, setIsLoading] = useState(true)
  const [isLoading2, setIsLoading2] = useState(true)

  useEffect(() => {
    const getLocation = async () => {
      if (!navigator.geolocation) {
        console.log('Geolocation is not supported.');
        return;
      } else {
        return await navigator.geolocation.getCurrentPosition((position) => {
          const { latitude, longitude } = position.coords
          setLocation({ latitude, longitude })
          getCity(latitude, longitude)
          getWeather(latitude, longitude)
        })
      }
    }
    getLocation()
  }, []);

  const getCity = async (lat, lng) => {
    //const lat = location.latitude
    //const lon = location.longitude
    try {
      let data = await fetch(`https://api.bigdatacloud.net/data/reverse-geocode-client?latitude=${lat}&longitude=${lng}&localityLanguage=en`)
      let dataJSON = await data.json()
      //console.log(dataJSON)
      setCity(dataJSON.locality)
      setIsLoading2(false)
    } catch (error) {
      console.log(error);
    }
  }

  const getWeather = async (lat, lng) => {
    //const lat = location.latitude
    //const lon = location.longitude
    try {
      let data = await fetch(`https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lng}&exclude=minutely&appid=16909a97489bed275d13dbdea4e01f59`)
      let dataJSON = await data.json()
      //console.log(dataJSON)
      
      setWeatherCurrent(dataJSON.current)

      let tempHourly = []
      for (let ele of dataJSON.hourly) {
        tempHourly.push(ele)
      }
      setWeatherHourly(tempHourly)

      let tempWeek = []
      for (let ele of dataJSON.daily) {
        tempWeek.push(ele)
      }
      setWeatherDaily(tempWeek)

      setIsLoading(false)
    } catch (error) {
      console.log(error)
    }
  }

  const getAutocomplete = async () => {
    try {
      let data = await fetch(`https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${text}&offset=3&key=AIzaSyBcisRmOieQ8xJPGkgIohDk8THFshk7yXU`)
      let dataJSON = await data.json()
      console.log(dataJSON)
      let tempPr = []
      for (let ele of dataJSON.predictions) {
        tempPr.push({desc: ele.description, term: ele.terms[0].value})
      }
      setPredictions(tempPr);
    } catch (error) {
      console.log(error)
    }
  }

  const getCoordinates = async (item) => {
    try {
      let data = await fetch(`https://maps.googleapis.com/maps/api/place/textsearch/json?query=${item}&key=AIzaSyBcisRmOieQ8xJPGkgIohDk8THFshk7yXU`)
      let dataJSON = await data.json()
      console.log(dataJSON)
      let lat = dataJSON.geometry.location.lat
      let lng = dataJSON.geometry.location.lng
      setLocation({ lat, lng })
      getWeather(lat, lng)
    } catch (error) {
      console.log(error)
    }
  }

  const onChangeText = (text) => {
    setText(text)
    getAutocomplete()
  }

  const onPressSearch = (item) => {
    setCity(item)
    getCoordinates(item)
  }

  return (
    <NavigationContainer>
      <View style={styles.container}>
        <ImageBackground source={require('./assets/sky.jpg')} style={styles.imageBack}>
          <View style={styles.content}>
            <View style={styles.modSearch}>
              <TextInput style={styles.input} onChangeText={(text) => onChangeText(text)} placeholder="Write a city" />
              {/*<Button title='Load' onPress={() => onPressSearch('London')}></Button>*/}
              <View style={styles.modPredictions}>
                {predictions.map((item, index) => {
                  return (
                    <Text style={styles.prediction} key={index} onPress={() => onPressSearch(item.term)}>{item.desc}</Text>
                  ) 
                })}
              </View>
            </View>

            { !isLoading && !isLoading2 ? 
            <View>
              <Image style={styles.iconWeather} source={{uri: `http://openweathermap.org/img/w/${weatherCurrent.weather[0].icon}.png`}} />
              <Text style={styles.descMain}>{weatherCurrent.weather[0].main}</Text>
              <Text style={styles.cityMain}>{city}</Text>
              <Text style={styles.tempMain}>{Math.ceil(weatherCurrent.temp-273)}º</Text>
            </View>
            : <Text style={styles.cityMain}>Loading...</Text> }

            <View>
              <ScrollView contentContainerStyle={styles.listHourly}>
                { 
                  weatherHourly.map((ele, i)=>(
                    <ItemHourly key={i} item={ele} />
                  )) 
                }
              </ScrollView>
            </View>

            <SafeAreaView>
              <FlatList data={weatherDaily} contentContainerStyle={styles.listDaily} renderItem={({ item }) => <Item item={item} />} keyExtractor={(item, idx) => String(idx)} />
            </SafeAreaView>
          </View>
        </ImageBackground>
      </View>
    </NavigationContainer>
  );
}

const ItemHourly = ({ item }) => {
  let celsius = Math.ceil(item.temp - 273)
  let icon = item.weather[0].icon
  let hour = item.dt
  return (
    <View style={styles.itemHourly}>
      <Text style={styles.descHourly}><Moment unix format='HH'>{hour}</Moment></Text>
      <Image style={styles.iconHourly} source={{uri: `http://openweathermap.org/img/w/${icon}.png`}} />
      <Text style={styles.descHourly}>{celsius}º</Text>
    </View>
  );
}

const Item = ({ item }) => {
  let day = item.dt
  let icon = item.weather[0].icon
  let celsiusMin = Math.ceil(item.temp.min - 273)
  let celsiusMax = Math.ceil(item.temp.max - 273)
  return (
    <View style={styles.itemDaily}>
      <Text style={[styles.colDailyFirst, styles.descDaily]}><Moment unix format='dddd'>{day}</Moment></Text>
      <View style={styles.colDaily}><Image style={styles.iconDaily} source={{uri: `http://openweathermap.org/img/w/${icon}.png`}} /></View>
      <Text style={[styles.colDaily, styles.descDaily]}>{celsiusMin}º</Text>
      <Text style={[styles.colDaily, styles.descDaily]}>{celsiusMax}º</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  },
  content: {
    width:'90%',
    marginLeft: 'auto',
    marginRight: 'auto',
    paddingTop: 40,
    paddingBottom: 40
  },
  imageBack: {
    width: '100%',
    height: '100vh'
  },
  cityMain: {
    fontSize: 24,
    color: '#fff',
    textAlign: 'center'
  },
  tempMain: {
    fontSize: 72,
    fontWeight: '200',
    color: '#fff',
    textAlign: 'center'
  },
  descMain: {
    fontSize: 14,
    color: '#fff',
    textAlign: 'center',
    marginTop: -15
  },
  iconWeather: {
    width: 64,
    height: 64,
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  input: {
    minHeight: 40,
    backgroundColor: 'rgba(255,255,255,1)',
    width: '100%',
    borderWidth: 2,
    borderColor: '#ddd',
    borderRadius: 2,
    paddingLeft: 5,
    paddingRight: 5
  },
  modSearch: {
    position: 'relative',
    zIndex: 90,
    marginBottom: 20
  },
  modPredictions: {
    width: '100%',
    position: 'absolute',
    top: 40,
    backgroundColor: 'white'
  },
  prediction: {
    color: 'black',
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 10,
    paddingRight: 10
  },
  listHourly: {
    flexDirection: 'row',
    borderTopColor: 'rgba(255,255,255,.25)',
    borderTopWidth: 1,
    paddingTop: 10,
    paddingBottom: 10,
    marginTop: 10
  },
  itemHourly: {
    marginRight: 5
  },
  descHourly: {
    fontSize: 14,
    color: '#fff',
    textAlign: 'center'
  },
  iconHourly: {
    width: 28,
    height: 28,
    marginTop:2,
    marginBottom: 2
  },
  listDaily: {
    borderTopColor: 'rgba(255,255,255,.25)',
    borderTopWidth: 1,
    paddingTop: 10
  },
  itemDaily: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  colDaily: {
    width: '20%'
  },
  colDailyFirst: {
    width: '40%'
  },
  descDaily: {
    fontSize: 14,
    color: '#fff'
  },
  iconDaily: {
    width: 28,
    height: 28
  }
});
